# Dotnet-Next

WIP Gentoo .NET overlay to support new
[dotnet-sdk](https://packages.gentoo.org/packages/virtual/dotnet-sdk).


## About

WARNING! This is currently broken, we have to find a way to prevent access to
"https://api.nuget.org/v3/index.json" and use self-compiled packages.
Currently ebuilds work only when used with the `ebuild` command.
