# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DOTNET_COMPAT=( 3.1 5.0 6.0 )

inherit dotnet-2 wrapper

DESCRIPTION="The compiler generator Coco/R"
HOMEPAGE="https://github.com/boogie-org/coco/"
SRC_URI="https://github.com/boogie-org/coco/archive/${PV}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}"/coco-${PV}

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"

DOCS=( README.md )

src_install() {
	dotnet-2_src_install

	make_wrapper coco "${DOTNET_PKG_DIR}"/${PN}/Coco
}
