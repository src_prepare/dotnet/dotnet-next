# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: dotnet-2.eclass
# @MAINTAINER:
# src_prepare group
# @AUTHOR:
# Maciej Barć <xgqt@riseup.net>
# @SUPPORTED_EAPIS: 7 8
# @BLURB: Common configuration eclass for compiling .NET core packages.
# @DESCRIPTION:
# This eclass is used in packages that depend on new .NET core SDK.

if [[ -z ${_DOTNET_2_ECLASS} ]] ; then
_DOTNET_2_ECLASS=1

inherit multiprocessing

case ${EAPI} in
	7 | 8 )  true  ;;
	* )  die "${ECLASS}: EAPI ${EAPI:-0} is not supported!"  ;;
esac

# @ECLASS_VARIABLE: _DOTNET_ALL_IMPLS
# @INTERNAL
# @DESCRIPTION:
# All supported .NET implementations, most preferred last.
_DOTNET_ALL_IMPLS=(
	3.1
	5.0
	6.0
)
readonly _DOTNET_ALL_IMPLS


# @FUNCTION: _dotnet_set_impls
# @INTERNAL
# @DESCRIPTION:
# Check DOTNET_COMPAT for well-formedness and validity, then set
# two global variables:
#
# - _DOTNET_SUPPORTED_IMPLS containing valid implementations supported
#   by the ebuild (DOTNET_COMPAT minus dead implementations),
#
# - and _DOTNET_UNSUPPORTED_IMPLS containing valid implementations that
#   are not supported by the ebuild.
#
# Implementations in both variables are ordered using the pre-defined
# eclass implementation ordering.
#
# This function must only be called once.
_dotnet_set_impls() {
	if ! declare -p DOTNET_COMPAT &>/dev/null ; then
		die "DOTNET_COMPAT not declared!"
	fi
	if [[ $(declare -p DOTNET_COMPAT) != "declare -a"* ]] ; then
		die "DOTNET_COMPAT must be an array!"
	fi

	local supp=( )
	local unsupp=( )

	# Walk thought _DOTNET_ALL_IMPLS and append each to either supp or unsupp.
	local impl
	for impl in ${_DOTNET_ALL_IMPLS[@]} ; do
		if has ${impl} ${DOTNET_COMPAT[@]} ; then
			supp+=( ${impl} )
		else
			unsupp+=( ${impl} )
		fi
	done

	if [[ ! ${supp[@]} ]] ; then
		die "No supported implementation in DOTNET_COMPAT."
	fi

	if [[ ${_DOTNET_SUPPORTED_IMPLS[@]} ]] ; then
		# Set once already, verify integrity.
		if [[ ${_DOTNET_SUPPORTED_IMPLS[@]} != ${supp[@]} ]] ; then
			eerror "Supported impls (DOTNET_COMPAT) changed between inherits!"
			eerror "Before: ${_DOTNET_SUPPORTED_IMPLS[*]}"
			eerror "Now   : ${supp[*]}"
			die "_DOTNET_SUPPORTED_IMPLS integrity check failed!"
		fi
		if [[ ${_DOTNET_UNSUPPORTED_IMPLS[@]} != ${unsupp[@]} ]] ; then
			eerror "Unsupported impls changed between inherits!"
			eerror "Before: ${_DOTNET_UNSUPPORTED_IMPLS[*]}"
			eerror "Now   : ${unsupp[*]}"
			die "_DOTNET_UNSUPPORTED_IMPLS integrity check failed!"
		fi
	else
		_DOTNET_SUPPORTED_IMPLS=( ${supp[@]} )
		_DOTNET_UNSUPPORTED_IMPLS=( ${unsupp[@]} )
		readonly _DOTNET_SUPPORTED_IMPLS
		readonly _DOTNET_UNSUPPORTED_IMPLS
	fi
}

# @FUNCTION: dotnet_translate_flag
# @DESCRIPTION:
# Translate a .NET implementation to a valid USE flag.
dotnet_translate_flag() {
	debug-print-function ${FUNCNAME} "${@}"

	[[ -z ${1} ]] && die "No version provided."

	echo "net${1/./_}"
}

# @FUNCTION: dotnet_translate_target
# @DESCRIPTION:
# Translate a .NET implementation to a framework target title.
dotnet_translate_target() {
	debug-print-function ${FUNCNAME} "${@}"

	case ${1} in
		3.1 ) echo netcoreapp3.1 ;;
		* ) echo "net${1}" ;;
	esac
}

# @FUNCTION: _dotnet_set_usedep
# @INTERNAL
# @DESCRIPTION:
# Setup for IUSE and DEPEND according to selected .NET version.
#
# This function must only be called once.
_dotnet_set_depend() {
	local impl
	local flag
	local dep
	for impl in ${_DOTNET_SUPPORTED_IMPLS[@]} ; do
		flag=$(dotnet_translate_flag ${impl})
		IUSE+=" ${flag} "
		dep="${flag}? ( virtual/dotnet-sdk:${impl} )"
		RDEPEND+=" ${dep} "
		BDEPEND+=" ${dep} "
	done

	IUSE+=" debug "
}

# @FUNCTION: dotnet-2_check_dotnet
# @DESCRIPTION:
# Die if the "dotnet" command is not found.
dotnet-2_check_dotnet() {
	debug-print-function ${FUNCNAME} "${@}"

	command -v dotnet >/dev/null ||
		die "the command \"dotnet\" is missing!"
}

# @FUNCTION: edotnet
# @DESCRIPTION:
# Wrapper for calling the "dotnet" binary.
edotnet() {
	debug-print-function ${FUNCNAME} "${@}"

	dotnet-2_check_dotnet

	ebegin "Invoking \"dotnet\" with arguments: \"${*}\""
	dotnet --diagnostics "${@}"
	eend ${?} "\"dotnet ${*}\" failed!" || die
}

# @FUNCTION: dotnet-2_prepare_environment
# @DESCRIPTION:
# Export correct DOTNET environment variables for build.
# About special environment variables:
# https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-environment-variables
dotnet-2_prepare_environment() {
	debug-print-function ${FUNCNAME} "${@}"

	# Disable telemetry
	DOTNET_CLI_TELEMETRY_OPTOUT=true
	export DOTNET_CLI_TELEMETRY_OPTOUT

	# Disable welcome messages
	DOTNET_NOLOGO=true
	export DOTNET_NOLOGO
	DOTNET_SKIP_FIRST_TIME_EXPERIENCE=true
	export DOTNET_SKIP_FIRST_TIME_EXPERIENCE

	# Do not generate ASP.NET certificates
	DOTNET_GENERATE_ASPNET_CERTIFICATE=false
	export DOTNET_GENERATE_ASPNET_CERTIFICATE
}

# @FUNCTION: dotnet-2_guard_use
# @DESCRIPTION:
# Check if exactly one .NET implementation is selected.
dotnet-2_guard_use() {
	debug-print-function ${FUNCNAME} "${@}"

	local selected=( )

	local impl
	for impl in ${_DOTNET_SUPPORTED_IMPLS[@]} ; do
		if use $(dotnet_translate_flag ${impl}) ; then
			selected+=( ${impl} )
		fi
	done

	if [[ ! ${selected[@]} ]] ; then
		die "No .NET implementation was selected!"
	elif [[ ${#selected[@]} != 1 ]] ; then
		die "Too many selected .NET implementations: ${selected[@]}!"
	fi
}

# @FUNCTION: dotnet-2_src_prepare
# @DESCRIPTION:
# Default function for the src_prepare phase.
dotnet-2_src_prepare() {
	debug-print-function ${FUNCNAME} "${@}"

	[[ -n "${DOTNET_ROOT}" ]] &&
		addpredict "${DOTNET_ROOT}"

	default
	dotnet-2_check_dotnet
	dotnet-2_prepare_environment
	dotnet-2_guard_use

	if use debug ; then
		DOTNET_CONFIGURATION=Debug
	else
		DOTNET_CONFIGURATION=Release
	fi
}

# @FUNCTION: dotnet-2_restore
# @DESCRIPTION:
# Restore the project.
dotnet-2_restore() {
	debug-print-function ${FUNCNAME} "${@}"

	local opts=(
		--no-cache
		--disable-parallel
	)

	echo "Restoring the project."
	edotnet restore ${opts[@]}
}

# @FUNCTION: dotnet-2_build
# @DESCRIPTION:
# Restore the project.
dotnet-2_build() {
	debug-print-function ${FUNCNAME} "${@}"

	local opts=(
		--configuration ${DOTNET_CONFIGURATION}
		--no-restore
		--nologo
	)

	local impl
	for impl in ${_DOTNET_SUPPORTED_IMPLS[@]} ; do
		if use $(dotnet_translate_flag ${impl}) ; then
			echo "Building for .NET SDK ${impl}."
			edotnet build ${opts[@]} \
				--framework $(dotnet_translate_target ${impl})
			break
		fi
	done
}

# @FUNCTION: dotnet-2_src_compile
# @DESCRIPTION:
# Default function for the src_compile phase.
dotnet-2_src_compile() {
	debug-print-function ${FUNCNAME} "${@}"

	dotnet-2_restore
	dotnet-2_build
}
# @FUNCTION: dotnet-2_copy_pkg
# @DESCRIPTION:
# Copy the sources to package installation directory.
dotnet-2_copy_pkg() {
	debug-print-function ${FUNCNAME} "${@}"

	local pkg_dir="${ED}"/${DOTNET_PKGS_DIR}/${PN}

	local impl
	local bdir
	for impl in ${_DOTNET_SUPPORTED_IMPLS[@]} ; do
		if use $(dotnet_translate_flag ${impl}) ; then
			bdir=bin/${DOTNET_CONFIGURATION}/$(dotnet_translate_target ${impl})
			mkdir -p ${pkg_dir} || die
			cp -r ${bdir}/* ${pkg_dir} || die
			break
		fi
	done
}

# @FUNCTION: dotnet-2_src_install
# @DESCRIPTION:
# Default function for the src_install phase.
dotnet-2_src_install() {
	debug-print-function ${FUNCNAME} "${@}"

	dotnet-2_copy_pkg

	# Install documentation when explicitly requested
	[[ $(declare -p DOCS 2>/dev/null) == *=* ]] &&
		einstalldocs
}

EXPORT_FUNCTIONS src_prepare src_compile src_install

_dotnet_set_impls
_dotnet_set_depend

IUSE+=" +net6_0 "

DOTNET_PKGS_DIR=/usr/share/dotnet/pkgs
QA_PREBUILT="*"

fi
